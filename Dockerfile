FROM alpine:edge

ENV S6_OVERLAY_VERSION v1.21.4.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Make some room for our friend flood and his older brother rtorrent
ENV FLOOD_HOME /var/www/flood
ENV RTORRENT_HOME /home/rtorrent

# Install main packages and build-dependencies and do some other stuff
RUN addgroup rtorrent \
    && adduser -D -s /bin/false -h "$RTORRENT_HOME" -G rtorrent rtorrent \
    && apk add --update --no-cache --no-progress bash coreutils curl rtorrent mediainfo nodejs nodejs-npm su-exec \
    && apk add --update --no-cache --no-progress --virtual .build-deps build-base git python2 \
    && npm install -g node-gyp \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && mkdir /run/rtorrent && chown rtorrent:rtorrent /run/rtorrent/ && chmod 750 /run/rtorrent/ \
    && git clone https://github.com/jfurrow/flood.git "$FLOOD_HOME" \
    && chown -R rtorrent:rtorrent "${FLOOD_HOME}" \
    && cd "$FLOOD_HOME" \
    && su-exec rtorrent cp "${FLOOD_HOME}/config.template.js" "${FLOOD_HOME}/config.js" \
    && su-exec rtorrent npm install \
    && su-exec rtorrent npm cache clean --force \
    && su-exec rtorrent npm rebuild node-sass \
    && su-exec rtorrent npm run build \
    && rm -rf .git .github \
    && apk del --purge .build-deps

# Copy the configuration file of rtorrent
COPY --chown=rtorrent:rtorrent ./config/.rtorrent.rc $RTORRENT_HOME

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

ENTRYPOINT ["/init"]
EXPOSE 3000
HEALTHCHECK --interval=30s --retries=3 CMD curl --fail http://localhost:3000 || exit 1
